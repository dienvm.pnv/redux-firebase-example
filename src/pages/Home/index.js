import React, { useState, useEffect } from 'react';
import { Container, Row, Col, Form, Button, Table } from 'react-bootstrap';

const Home = ({
  loading,
  products,
  createProductRequest,
  fetchProductRequest,
  type,
}) => {
  const [fields, updateFields] = useState({});

  const onSubmit = () => {
    createProductRequest(fields);
  };

  const handleChangeText = (text, field) => {
    updateFields({ ...fields, [field]: text });
  };

  useEffect(() => {
    fetchProductRequest();
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, []);

  useEffect(() => {
    if (type === 'CREATE_PRODUCT_SUCCESS') {
      fetchProductRequest();
    }
  }, [type, fetchProductRequest]);

  if (loading) {
    return (
      <Container>
        <Row>
          <Col>
            <h5>Loading...</h5>
          </Col>
        </Row>
      </Container>
    );
  }

  return (
    <Container>
      <h2>List Product</h2>
      <Table striped bordered hover>
        <thead>
          <tr>
            <th>#</th>
            <th>Title</th>
            <th>Category</th>
            <th>Content</th>
          </tr>
        </thead>
        <tbody>
          {!!products &&
            products.length > 0 &&
            products.map((product, index) => {
              return (
                <tr key={index}>
                  <td>{index}</td>
                  <td>{product.title}</td>
                  <td>{product.category}</td>
                  <td>{product.content}</td>
                </tr>
              );
            })}
        </tbody>
      </Table>

      <Row>
        <Container>
          <Form.Group controlId='productTitle'>
            <Form.Label>Title For Product</Form.Label>
            <Form.Control
              type='text'
              placeholder='Title For Product'
              onChange={(event) =>
                handleChangeText(event.target.value, 'title')
              }
            />
          </Form.Group>

          <Form.Group controlId='categories'>
            <Form.Label>Categories</Form.Label>
            <Form.Control
              type='text'
              placeholder='categories'
              onChange={(event) =>
                handleChangeText(event.target.value, 'category')
              }
            />
          </Form.Group>

          <Form.Group controlId='productContent'>
            <Form.Label>Product Content'</Form.Label>
            <Form.Control
              type='text'
              placeholder='Product content'
              onChange={(event) =>
                handleChangeText(event.target.value, 'content')
              }
            />
          </Form.Group>

          <Button variant='primary' onClick={onSubmit}>
            Submit
          </Button>
        </Container>
      </Row>
    </Container>
  );
};

export default Home;
