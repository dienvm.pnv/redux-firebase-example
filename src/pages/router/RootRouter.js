import React from 'react';
import { BrowserRouter, Route, Switch } from 'react-router-dom';

import HomeContainer from '../../containers/HomeContainer';

import { Container } from 'react-bootstrap';

const RootRouter = () => {
  return (
    <BrowserRouter>
      <Container>
        <Switch>
          <Route path='/' component={HomeContainer} />
        </Switch>
      </Container>
    </BrowserRouter>
  );
};

export default RootRouter;
