import { connect } from 'react-redux';
import { Creators } from '../../actions/productAction';
import Home from '../../pages/Home';

const mapStateToProps = (state) => ({
  products: state.product.products,
  product: state.product.product,
  loading: state.product.loading,
  error: state.product.error,
  type: state.product.type,
});

const mapDispatchToProps = {
  ...Creators,
};

const PostNewContainer = connect(mapStateToProps, mapDispatchToProps)(Home);

export default PostNewContainer;
